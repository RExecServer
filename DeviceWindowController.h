//
//  DeviceWindowController.h
//  RExecServer
//
//  Created by Byron Ellis on 6/29/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DeviceView.h"
#import "RDevice.h"

@interface DeviceWindowController : NSWindowController<RDeviceDelegate> {
	IBOutlet DeviceView *deviceView;
	RDevice *device;
}

- (void)setDevice:(RDevice*)aDevice;

@end
