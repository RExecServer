#import "RDevice.h"
#import "NSData+RSerialize.h"

CGContextRef RDevice_GetCGContext(QuartzDesc_t,void*);
int          RDevice_LocatePoint(QuartzDesc_t,void*,double*,double*);
void         RDevice_GetSize(QuartzDesc_t,void*,double*,double*);
void         RDevice_Close(QuartzDesc_t,void*);
void		 RDevice_NewPage(QuartzDesc_t desc,void*ui);
void		 RDevice_Activate(QuartzDesc_t desc,void*ui,int num);
void		 RDevice_Deactivate(QuartzDesc_t desc,void*ui,int num);
static NSMutableDictionary *deviceDict = nil;

@implementation RDevice

+ (Class)deviceForDisplay:(NSString*)display {
	if(nil == deviceDict) deviceDict = [[NSMutableDictionary alloc] init];
	return [deviceDict objectForKey:display];
}

+ (void)registerDevice:(Class)deviceClass forDisplay:(NSString*)display {
	if(nil == deviceDict) deviceDict = [[NSMutableDictionary alloc] init];
	[deviceDict setObject:deviceClass forKey:display];
}
- (double)defaultScale { return 1.0; }

- (id)initWithDevice:(void*)dd size:(NSSize)size pointSize:(double)ps display:(NSString*)aDisplay target:(NSString*)aTarget 
	background:(int)bg antialias:(BOOL)antialias {

	if(nil == [super init]) return nil;
	desc = RDevice_Create(dd,[self defaultScale],ps,size.width,size.height,bg,antialias,
		RDevice_GetCGContext,
		([self canLocate] == YES) ? RDevice_LocatePoint : NULL,
		RDevice_Close,
		RDevice_NewPage,
		RDevice_Activate,
		RDevice_Deactivate,
	(void*)self);
	display = [aDisplay retain];
	target  = [aTarget retain];
	return self;
}

- (void)dealloc {
	[display release];
	if(nil != target) [target release];
	[super dealloc];
}

- (NSString*)display { return display; }
- (NSString*)target  { return target;  }

- (BOOL)canResize { return NO; }
- (BOOL)canLocate { return NO; }
- (BOOL)canDrawInView { return NO; }

- (CGContextRef)context { return NULL; }
- (BOOL)locatePointAtX:(double*)x y:(double*)y { *x = *y = 0.0;return NO; }
- (void)close  { 
	if(nil != delegate) {
		[delegate didEndPageForDevice:self];
		[delegate willCloseDevice:self];
	}
	desc = NULL; 
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:@"RDeviceClosed" object:self]];
}
- (void)newPage { 
	if(nil != delegate) {
		[delegate didEndPageForDevice:self];
		[delegate willBeginPageForDevice:self];
	}
}
- (void)activate:(int)devNum { 
	if(nil != delegate) [delegate didActivateDevice:self withNumber:devNum];
}
- (void)deactivate:(int)devNum { 
	if(nil != delegate) [delegate didDeactivateDevice:self withNumber:devNum];
}

- (NSSize)size { 
	double sc = 72.0*RDevice_GetScale(desc);
	return NSMakeSize(sc*RDevice_GetWidth(desc),sc*RDevice_GetHeight(desc));
}

- (void)didResize { }

- (void)setSize:(NSSize)newSize {
	double sc = 72.0*RDevice_GetScale(desc);
	if(abs(newSize.width/sc - RDevice_GetWidth(desc)) > .0001 || abs(newSize.height/sc - RDevice_GetHeight(desc)) > .0001) {
		RDevice_SetWidth(desc,newSize.width/sc);
		RDevice_SetHeight(desc,newSize.height/sc);
		[self didResize];
	}
}

- (id<RDeviceDelegate>)delegate { return delegate; }
- (void)setDelegate:(id<RDeviceDelegate>)aDelegate {
	if(nil != delegate)
		[delegate willChangeDelegateForDevice:self];
	delegate = aDelegate;
	if(nil != delegate) {
		[delegate didChangeDelegateForDevice:self];
		[delegate didUpdateDevice:self];
	}
}

- (void)drawInRect:(NSRect)aRect { }

- (void)finishOpening { }
- (void)flushDrawing  { 
	if(RDevice_GetDirty(desc)) {
		if(nil != delegate)
			[delegate didUpdateDevice:self];
		RDevice_SetDirty(desc,0);
	}
}

- (void)redraw { RDevice_ReplayDisplayList(desc);[delegate didUpdateDevice:self]; }

- (NSData*)deviceRepresentationOfType:(NSString*)aType {
	return nil;
}

- (bycopy NSData*)snapshot {
	return [NSData dataWithSEXP:RDevice_GetSnapshot(desc)];
	
}
- (void)setSnapshot:(NSData*)aData {
	NSLog(@"Got snapshot data with length:%d",[aData length]);
	RDevice_RestoreSnapshot(desc, [aData unserialize]);
	if(nil != delegate)
		[delegate didUpdateDevice:self];
}



@end

CGContextRef RDevice_GetCGContext(QuartzDesc_t desc,void*ui) { 
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	CGContextRef ctx = [(RDevice*)ui context];
	[pool release];
	return ctx;
}
int          RDevice_LocatePoint(QuartzDesc_t desc,void*ui,double*x,double *y) { 
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	int ret = [(RDevice*)ui locatePointAtX:x y:y]; 
	[pool release];
	return ret;
}
void         RDevice_Close(QuartzDesc_t desc,void*ui) { 
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];	
	[(RDevice*)ui close]; 
	[pool release];
}

void RDevice_NewPage(QuartzDesc_t desc,void*ui) {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];	
	[(RDevice*)ui newPage]; 
	[pool release];
}

void RDevice_Activate(QuartzDesc_t desc,void*ui,int num) {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];	
	[(RDevice*)ui activate:num]; 
	[pool release];
}

void RDevice_Deactivate(QuartzDesc_t desc,void*ui,int num) {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];	
	[(RDevice*)ui deactivate:num]; 
	[pool release];
}

