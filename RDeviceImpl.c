#include "RDeviceImpl.h"
#include <Rinternals.h>
#include <Rgraphics.h>
#include <Rdevices.h>

#include <R_ext/GraphicsDevice.h>
#include <R_ext/GraphicsEngine.h>

typedef struct {
	double ps,scale,width,height,tscale;
	int            dirty,bg,gstate,antialias,redraw;
	CGRect         clipRect;
	NewDevDesc    *dev; //Circular reference.
	void*          userInfo;
	CGContextRef (*getCGContext)(QuartzDesc_t dev,void*userInfo);
	int          (*locatePoint)(QuartzDesc_t dev,void*userInfo,double*x,double*y);
	void         (*close)(QuartzDesc_t dev,void*userInfo);
	void         (*newPage)(QuartzDesc_t dev,void*userInfo);
	void         (*activate)(QuartzDesc_t dev,void*userInfo,int devNum);
	void         (*deactivate)(QuartzDesc_t dev,void*userInfo,int devNum);
} QuartzDesc;


#pragma mark QuartzDesc manipulation
#pragma mark -

void RDevice_Update(QuartzDesc_t desc);

double RDevice_ScaledWidth(QuartzDesc *qd)   { return qd->scale*qd->width*72.0; }
double RDevice_ScaledHeight(QuartzDesc *qd)  { return qd->scale*qd->height*72.0; }


double RDevice_GetWidth(QuartzDesc_t desc)				{ return ((QuartzDesc*)desc)->width;  }
void   RDevice_SetWidth(QuartzDesc_t desc,double width) { ((QuartzDesc*)desc)->width = width;((QuartzDesc*)desc)->dev->right = RDevice_ScaledWidth(desc); }

double RDevice_GetHeight(QuartzDesc_t desc)				  { return ((QuartzDesc*)desc)->height;   }
void   RDevice_SetHeight(QuartzDesc_t desc,double height) { ((QuartzDesc*)desc)->height = height;((QuartzDesc*)desc)->dev->bottom = RDevice_ScaledHeight(desc); }

double RDevice_GetScale(QuartzDesc_t desc)					{ return ((QuartzDesc*)desc)->scale;  }
void   RDevice_SetScale(QuartzDesc_t desc,double scale)		{ ((QuartzDesc*)desc)->scale = scale;RDevice_Update(desc); }

double RDevice_GetTextScale(QuartzDesc_t desc)					{ return ((QuartzDesc*)desc)->tscale;  }
void   RDevice_SetTextScale(QuartzDesc_t desc,double scale)		{ ((QuartzDesc*)desc)->tscale = scale;RDevice_Update(desc); }


int   RDevice_GetDirty(QuartzDesc_t desc) { return ((QuartzDesc*)desc)->dirty; }
void  RDevice_SetDirty(QuartzDesc_t desc,int dirty) { ((QuartzDesc*)desc)->dirty; }

int   RDevice_GetGDepth(QuartzDesc_t desc) { return ((QuartzDesc*)desc)->gstate; }
void  RDevice_ResetGDepth(QuartzDesc_t desc) { ((QuartzDesc*)desc)->gstate = 0; }

int   RDevice_GetAntialias(QuartzDesc_t desc) { return ((QuartzDesc*)desc)->antialias; }


void   RDevice_Update(QuartzDesc_t desc) {
	QuartzDesc *qd = (QuartzDesc*)desc;
	NewDevDesc *dev= qd->dev;
	
	dev->cra[0] = 0.9*qd->scale*qd->ps*qd->tscale;
	dev->cra[1] = 1.2*qd->scale*qd->ps*qd->tscale;
	dev->ipr[0] = dev->ipr[1] = 1.0/(72.0*qd->scale);
}

void RDevice_ReplayDisplayList(QuartzDesc_t desc) {
	QuartzDesc *qd = (QuartzDesc*)desc;
	qd->redraw = 1;
	if(qd->dev->displayList != R_NilValue)
		GEplayDisplayList((GEDevDesc*)GetDevice(devNumber((DevDesc*)qd->dev)));
	qd->redraw = 0;
}

void* RDevice_GetSnapshot(QuartzDesc_t desc) {
	QuartzDesc *qd = (QuartzDesc*)desc;
	return qd->dev->savedSnapshot;
}

void RDevice_RestoreSnapshot(QuartzDesc_t desc,void* snap) {
	QuartzDesc *qd = (QuartzDesc*)desc;
	PROTECT((SEXP)snap);
	qd->redraw = 1;
	GEplaySnapshot((SEXP)snap,(GEDevDesc*)GetDevice(devNumber((DevDesc*)qd->dev)));
	qd->redraw = 0;
	UNPROTECT(1);
}

#pragma mark Function Prototypes

static Rboolean RQuartz_Open(NewDevDesc*,QuartzDesc*,char*,double,double,int);
static void     RQuartz_Close(NewDevDesc*);
static void     RQuartz_Activate(NewDevDesc*);
static void     RQuartz_Deactivate(NewDevDesc*);
static void     RQuartz_Size(double*,double*,double*,double*,NewDevDesc*);
static void     RQuartz_NewPage(R_GE_gcontext*,NewDevDesc*);
static void     RQuartz_Clip(double,double,double,double,NewDevDesc*);
static double   RQuartz_StrWidth(char*,R_GE_gcontext*,NewDevDesc*);
static void     RQuartz_Text(double,double,char*,double,double,R_GE_gcontext*,NewDevDesc*);
static void     RQuartz_Rect(double,double,double,double,R_GE_gcontext*,NewDevDesc*);
static void     RQuartz_Circle(double,double,double,R_GE_gcontext*,NewDevDesc*);
static void     RQuartz_Line(double,double,double,double,R_GE_gcontext*,NewDevDesc*);
static void     RQuartz_Polyline(int,double*,double*,R_GE_gcontext*,NewDevDesc*);
static void     RQuartz_Polygon(int,double*,double*,R_GE_gcontext*,NewDevDesc*);
static Rboolean RQuartz_Locator(double*,double*,NewDevDesc*);
static void     RQuartz_Mode(int mode,NewDevDesc*);
static void     RQuartz_Hold(NewDevDesc*);
static void     RQuartz_MetricInfo(int,R_GE_gcontext *,double*,double*,double*,NewDevDesc*);


void* RDevice_Create(
	void *_dev,double scale,double ps,double width,double height,int bg,int aa,
	CGContextRef (*getCGContext)(QuartzDesc_t dev,void*userInfo), //Get the context for this device
	int          (*locatePoint)(QuartzDesc_t dev,void*userInfo,double*x,double*y),
	void         (*close)(QuartzDesc_t dev,void*userInfo),
	void         (*newPage)(QuartzDesc_t dev,void*userInfo),
	void         (*activate)(QuartzDesc_t dev,void*userInfo,int devNum),
	void         (*deactivate)(QuartzDesc_t dev,void*userInfo,int devNum),
	void*userInfo) {
		NewDevDesc *dev = (NewDevDesc*)_dev;
		dev->displayList = R_NilValue;
        
		dev->startfill = R_RGB(255,255,255);
		dev->startcol  = R_RGB(0,0,0);
		dev->startps   = ps;
        dev->startfont = 1;
        dev->startlty  = LTY_SOLID;
        dev->startgamma= 1;

        //Set up some happy pointers
        dev->newDevStruct = 1;
        dev->open         = RQuartz_Open;
        dev->close        = RQuartz_Close;
        dev->activate     = RQuartz_Activate;
        dev->deactivate   = RQuartz_Deactivate;
        dev->size         = RQuartz_Size;
        dev->newPage      = RQuartz_NewPage;
        dev->clip         = RQuartz_Clip;
        dev->strWidth     = RQuartz_StrWidth;
        dev->text         = RQuartz_Text;
        dev->rect         = RQuartz_Rect;
        dev->circle       = RQuartz_Circle;
        dev->line         = RQuartz_Line;
        dev->polyline     = RQuartz_Polyline;
        dev->polygon      = RQuartz_Polygon;
        dev->locator      = RQuartz_Locator;
        dev->mode         = RQuartz_Mode;
        dev->hold         = RQuartz_Hold;
        dev->metricInfo   = RQuartz_MetricInfo;
	
		dev->left = 0;
		dev->top  = 0;
		

		//Magic numbers from on high.
        dev->xCharOffset = 0.4900;
        dev->yCharOffset = 0.3333;
        dev->yLineBias   = 0.20; //This is .2 for PS/PDF devices...

		dev->canResizePlot = TRUE;
		dev->canChangeFont = TRUE;
		dev->canRotateText = TRUE;
		dev->canResizeText = TRUE;
		dev->canClip       = TRUE;
		dev->canHAdj       = 2;
		dev->canChangeGamma= TRUE;
		dev->displayListOn = TRUE;       
		
		QuartzDesc *qd = calloc(1,sizeof(QuartzDesc));
		qd->width      = width;
		qd->height     = height;
		qd->userInfo   = userInfo;
		qd->getCGContext = getCGContext;
		qd->locatePoint= locatePoint;
		qd->close      = close;
		qd->newPage    = newPage;
		qd->activate   = activate;
		qd->deactivate = deactivate;
		qd->scale = scale;
		qd->tscale= 1.0;
		qd->ps    = ps;
		qd->bg    = bg;
		qd->antialias = aa;
		qd->gstate= 0;
		
		dev->deviceSpecific = qd;
		qd->dev             = dev;
		
		
		RDevice_Update(qd);

		dev->right = RDevice_ScaledWidth(qd)-0.0001;
		dev->bottom= RDevice_ScaledHeight(qd)-0.0001;
		qd->clipRect = CGRectMake(0,0,dev->right,dev->bottom);

		qd->dirty = 0;
		qd->redraw= 0;
		return (QuartzDesc_t)qd;
}

#if MAC_OS_X_VERSION_MAX_ALLOWED <= MAC_OS_X_VERSION_10_4
extern CGFontRef CGFontCreateWithName(CFStringRef);
#define CGFontCreateWithFontName CGFontCreateWithName
#define CGFontGetGlyphBBoxes CGFontGetGlyphBoundingBoxes
#define CGFontGetGlyphsForUnichars CGFontGetGlyphsForUnicodes
#endif
//Always needs this one...
extern CGFontRef CGContextGetFont(CGContextRef);


#define DEVDESC NewDevDesc *dd
#define CTXDESC R_GE_gcontext*gc,NewDevDesc*dd

#define DEVSPEC QuartzDesc *xd = (QuartzDesc*)dd->deviceSpecific;CGContextRef ctx = xd->getCGContext(xd,xd->userInfo)
#define DRAWSPEC QuartzDesc *xd = (QuartzDesc*)dd->deviceSpecific;CGContextRef ctx = xd->getCGContext(xd,xd->userInfo);xd->dirty = 1;if(NULL==ctx) { printf("Invalid Context\n");return; }
#define XD QuartzDesc *xd = (QuartzDesc*)dd->deviceSpecific
#pragma mark Device Implementation

CFStringRef RQuartz_FindFont(int fontface,char *fontfamily) {
        SEXP ns,env,db,names;
        PROTECT_INDEX index;
        CFStringRef fontName = CFSTR("");
        PROTECT(ns = R_FindNamespace(ScalarString(mkChar("grDevices"))));
        PROTECT_WITH_INDEX(env = findVar(install(".Quartzenv"),ns),&index);
        if(TYPEOF(env) == PROMSXP)
                REPROTECT(env = eval(env,ns),index);
        PROTECT(db    = findVar(install(".Quartz.Fonts"),env));
        PROTECT(names = getAttrib(db,R_NamesSymbol));
        if(strlen(fontfamily)>0) {
                int i;
                for(i=0;i<length(names);i++)
                        if(0 == strcmp(fontfamily,CHAR(STRING_ELT(names,i)))) break;
                if(i<length(names))
                        fontName = CFStringCreateWithCString(kCFAllocatorDefault,CHAR(STRING_ELT(VECTOR_ELT(db,i),fontface)), kCFStringEncodingUTF8);
        }
        UNPROTECT(4);
        return fontName;
}

CGFontRef RQuartz_Font(CTXDESC) {
        int fontface = gc->fontface;
        CFMutableStringRef fontName = CFStringCreateMutable(kCFAllocatorDefault,0);
        if((gc->fontface == 5) || (strcmp(gc->fontfamily,"symbol") == 0)) 
                CFStringAppend(fontName,CFSTR("Symbol"));
        else {
                CFStringRef font = RQuartz_FindFont(gc->fontface,gc->fontfamily);
                if(CFStringGetLength(font)>0) {
                        fontface = 1; //This is handled by the lookup process           
                        CFStringAppend(fontName,font);
                }
                CFRelease(font);
        }
        if(CFStringGetLength(fontName) == 0) 
			CFStringAppend(fontName,CFSTR("Arial"));
        if(fontface==2 || fontface == 4) {
                CFStringAppend(fontName,CFSTR(" Bold"));
        }
        if(fontface==3) {
                CFStringAppend(fontName,CFSTR(" Italic"));
        }
        CGFontRef  font    = CGFontCreateWithFontName(fontName);
		if(font == 0) {
			//Fall back on ATS
			ATSFontRef tmp = ATSFontFindFromName(fontName,kATSOptionFlagsDefault);
			font = CGFontCreateWithPlatformFont(&tmp);
		}
		if(NULL == font) {
			CFShow(fontName);
		}
        CFRelease(fontName);
        return font;
}

#define RQUARTZ_FILL   (1)
#define RQUARTZ_STROKE (1<<1)
#define RQUARTZ_LINE   (1<<2)
#define RQUARTZ_FONT   (1<<3)

void RQuartz_Set(CGContextRef ctx,R_GE_gcontext*gc,int flags) { 
        if(flags & RQUARTZ_FILL) {
                int fill = gc->fill;
                CGContextSetRGBFillColor(ctx,R_RED(fill)/256.0,R_GREEN(fill)/256.0,R_BLUE(fill)/256.0,R_ALPHA(fill)/256.0);
        }
        if(flags & RQUARTZ_STROKE) {
                int stroke = gc->col;
                CGContextSetRGBStrokeColor(ctx,R_RED(stroke)/256.0,R_GREEN(stroke)/256.0,R_BLUE(stroke)/256.0,R_ALPHA(stroke)/256.0);
        }
        if(flags & RQUARTZ_LINE) {
                float dashlist[8];
                int   i,ndash = 0;
                int   lty = gc->lty;
                CGContextSetLineWidth(ctx,gc->lwd);
                
                float lwd = gc->lwd*0.75;
                for(i=0;i<8 && lty;i++) {
                        dashlist[ndash++] = (lwd >= 1 ? lwd : 1)*(lty&15);
                        lty >>= 4;
                }
                CGContextSetLineDash(ctx,0,dashlist,ndash);
                CGLineCap cap;
                switch(gc->lend) {
                case GE_ROUND_CAP:cap = kCGLineCapRound;break;
                case GE_BUTT_CAP:cap = kCGLineCapButt;break;
                case GE_SQUARE_CAP:cap = kCGLineCapSquare;break;
                }
                CGContextSetLineCap(ctx,cap);
                CGLineJoin join;
                switch(gc->ljoin) {
                case GE_ROUND_JOIN:join = kCGLineJoinRound;break;
                case GE_MITRE_JOIN:join = kCGLineJoinMiter;break;
                case GE_BEVEL_JOIN:join = kCGLineJoinBevel;break;
                }
                CGContextSetLineJoin(ctx,join);
                CGContextSetMiterLimit(ctx,gc->lmitre);
        }
        if(flags & RQUARTZ_FONT) {
                CGFontRef font = RQuartz_Font(gc,NULL);
                CGContextSetFont(ctx,font);
                CGContextSetFontSize(ctx,gc->cex*gc->ps);
        }
}

#define SET(X) RQuartz_Set(ctx,gc,(X))

static Rboolean RQuartz_Open(DEVDESC,QuartzDesc *xd,char *display,double width,double height,int bg) {
        //We don't do anything here.
        return TRUE;
}

static void RQuartz_Close(DEVDESC) {
		XD;
		if(NULL != xd->close) xd->close(xd,xd->userInfo);
}

static void RQuartz_Activate(DEVDESC) {
	XD;
	if(NULL != xd->activate) xd->activate(xd,xd->userInfo,devNumber((DevDesc*)xd->dev));
}

static void RQuartz_Deactivate(DEVDESC) {
	XD;
	if(NULL != xd->deactivate) xd->deactivate(xd,xd->userInfo,devNumber((DevDesc*)xd->dev));
}

static void RQuartz_Size(double*left,double*right,double*bottom,double*top,DEVDESC) {
	XD;
    *left = *top = 0;
	*right  = RDevice_ScaledWidth(xd);
	*bottom = RDevice_ScaledHeight(xd);
}

static void RQuartz_NewPage(CTXDESC) {
        DRAWSPEC;
		//Should have new page event
		if(0 == xd->redraw && NULL != xd->newPage) xd->newPage(xd,xd->userInfo);
        SET(RQUARTZ_FILL);
        CGRect bounds = CGRectMake(0,0,RDevice_ScaledWidth(xd),RDevice_ScaledHeight(xd));
		if(R_ALPHA(xd->bg) == 255 && R_ALPHA(gc->fill) == 255)
			CGContextClearRect(ctx,bounds);
        CGContextFillRect(ctx,bounds);
}

static void RQuartz_Clip(double x0,double x1,double y0,double y1,DEVDESC) {
	DRAWSPEC;
	if(xd->gstate > 0) {
		--xd->gstate;
		CGContextRestoreGState(ctx);
	}
	CGContextSaveGState(ctx);
	xd->gstate++;
	if(x1 > x0) { double t = x1;x1 = x0;x0 = t; }
	if(y1 > y0) { double t = y1;y1 = y0;y0 = t; }
	xd->clipRect = CGRectMake(x0,y0,x1-x0,y1-y0);
	CGContextClipToRect(ctx,xd->clipRect);
}

CFStringRef prepareText(CTXDESC,char *text,UniChar **buffer,int *free) {
        CFStringRef str;
        if(gc->fontface == 5 || strcmp(gc->fontfamily,"symbol") == 0)
                str = CFStringCreateWithCString(NULL,text,kCFStringEncodingMacSymbol);
        else {
                str = CFStringCreateWithCString(NULL,text,kCFStringEncodingUTF8);
                //Try fallback string encodings if UTF8 doesn't work.
                if(NULL == str)
                        CFStringCreateWithCString(NULL,text,kCFStringEncodingISOLatin1);
        }
        *buffer = (UniChar*)CFStringGetCharactersPtr(str);
        if (*buffer == NULL) {
                CFIndex length = CFStringGetLength(str);
                *buffer = malloc(length * sizeof(UniChar));
                CFStringGetCharacters(str, CFRangeMake(0, length), *buffer);
                *free = 1;
        }
        return str;
}

static double RQuartz_StrWidth(char *text,CTXDESC) {
        DEVSPEC;
        if(ctx==NULL) return 0.0;
        SET(RQUARTZ_FONT);
		CGFontRef font = CGContextGetFont(ctx);
        float aScale   = (gc->cex*gc->ps*xd->tscale)/CGFontGetUnitsPerEm(font);
        UniChar *buffer;
        CGGlyph   *glyphs;
        int      *advances;
        int Free = 0,len,i;
        CFStringRef str = prepareText(gc,dd,text,&buffer,&Free);
        len = CFStringGetLength(str);
        glyphs = malloc(sizeof(CGGlyph)*len);
        advances = malloc(sizeof(int)*len);
        CGFontGetGlyphsForUnichars(font,buffer,glyphs,len);
        CGFontGetGlyphAdvances(font,glyphs,len,advances);
        float width = 0.0;// aScale*CGFontGetLeading(CGContextGetFont(ctx));
        for(i=0;i<len;i++) width += aScale*advances[i];
        free(advances);
        free(glyphs);
        if(Free) free(buffer);
        CFRelease(str);
        return width;
}

static void RQuartz_Text(double x,double y,char *text,double rot,double hadj,CTXDESC) {
        DRAWSPEC;
        //A stupid hack because R isn't consistent.
        int fill = gc->fill;
        gc->fill = gc->col;
        SET(RQUARTZ_FILL|RQUARTZ_STROKE|RQUARTZ_FONT);
        gc->fill = fill;
        CGFontRef font = CGContextGetFont(ctx);
        float aScale   = (gc->cex*gc->ps*xd->tscale)/(CGFontGetUnitsPerEm(font));
        UniChar *buffer;
        CGGlyph   *glyphs;

        int Free = 0,len,i;
        float width = 0.0;
        CFStringRef str = prepareText(gc,dd,text,&buffer,&Free);
        len = CFStringGetLength(str);
        glyphs = malloc(sizeof(CGGlyph)*len);
        CGFontGetGlyphsForUnichars(font,buffer,glyphs,len);
        int      *advances = malloc(sizeof(int)*len);
        CGSize   *g_adv    = malloc(sizeof(CGSize)*len);
        
        CGFontGetGlyphAdvances(font,glyphs,len,advances);
        for(i=0;i<len;i++) { width += advances[i]*aScale;g_adv[i] = CGSizeMake(aScale*advances[i]*cos(-0.0174532925*rot),aScale*advances[i]*sin(-0.0174532925*rot)); }
        free(advances);
        CGContextSetTextMatrix(ctx,CGAffineTransformConcat(CGAffineTransformMakeScale(1.0,-1.0),CGAffineTransformMakeRotation(-0.0174532925*rot)));
        double ax = (width*hadj)*cos(-0.0174532925*rot);
        double ay = (width*hadj)*sin(-0.0174532925*rot);
//      double h  = CGFontGetXHeight(CGContextGetFont(ctx))*aScale;
        CGContextSetTextPosition(ctx,x-ax,y-ay);
//      Rprintf("%s,%.2f %.2f (%.2f,%.2f) (%d,%f)\n",text,hadj,width,ax,ay,CGFontGetUnitsPerEm(CGContextGetFont(ctx)),CGContextGetFontSize(ctx));       
        CGContextShowGlyphsWithAdvances(ctx,glyphs,g_adv,len);
        free(glyphs);
        free(g_adv);
        if(Free) free(buffer);
        CFRelease(str);
}

static void RQuartz_Rect(double x0,double y0,double x1,double y1,CTXDESC) {
        DRAWSPEC;
        SET(RQUARTZ_FILL|RQUARTZ_STROKE|RQUARTZ_LINE);
        CGContextBeginPath(ctx);
        CGContextAddRect(ctx,CGRectMake(x0,y0,x1-x0,y1-y0));
        CGContextDrawPath(ctx,kCGPathFillStroke);
}

static void RQuartz_Circle(double x,double y,double r,CTXDESC) {
        DRAWSPEC;
        SET(RQUARTZ_FILL|RQUARTZ_STROKE|RQUARTZ_LINE);
        double r2 = 2.0*r;
        CGContextBeginPath(ctx);
        CGContextAddEllipseInRect(ctx,CGRectMake(x-r,y-r,r2,r2));
        CGContextDrawPath(ctx,kCGPathFillStroke);
}

static void RQuartz_Line(double x1,double y1,double x2,double y2,CTXDESC) {
        DRAWSPEC;
        SET(RQUARTZ_STROKE|RQUARTZ_LINE);
        CGContextBeginPath(ctx);
        CGContextMoveToPoint(ctx,x1,y1);
        CGContextAddLineToPoint(ctx,x2,y2);
        CGContextStrokePath(ctx);
}

static void RQuartz_Polyline(int n,double *x,double *y,CTXDESC) {
        if(n<2) return; 
        int i;
        DRAWSPEC;
        SET(RQUARTZ_STROKE|RQUARTZ_LINE);       
        CGContextBeginPath(ctx);
        CGContextMoveToPoint(ctx,x[0],y[0]);
        for(i=1;i<n;i++) CGContextAddLineToPoint(ctx,x[i],y[i]);
        CGContextStrokePath(ctx);
}

static void RQuartz_Polygon(int n,double *x,double *y,CTXDESC) {
        if(n<2) return; 
        int i;
        DRAWSPEC;
        SET(RQUARTZ_FILL|RQUARTZ_STROKE|RQUARTZ_LINE);  
        CGContextBeginPath(ctx);
        CGContextMoveToPoint(ctx,x[0],y[0]);
        for(i=1;i<n;i++) CGContextAddLineToPoint(ctx,x[i],y[i]);
        CGContextClosePath(ctx);
        CGContextDrawPath(ctx,kCGPathFillStroke);       
}

static void RQuartz_Mode(int mode,DEVDESC) {
        DEVSPEC;
        if(0 == mode) { 
			if(xd->gstate > 0) {
				--xd->gstate;
				CGContextRestoreGState(ctx);
			}
			CGContextSynchronize(ctx); 
		} else {
			//Set the clipping rect for this operation.
			if(xd->gstate > 0) {
				--xd->gstate;
				CGContextRestoreGState(ctx);
			}			
			++xd->gstate;
			CGContextSaveGState(ctx);
			CGContextClipToRect(ctx,xd->clipRect);
		}
}

static void RQuartz_Hold(DEVDESC) {
}

static void RQuartz_MetricInfo(int c,R_GE_gcontext *gc,double *ascent,double *descent,double *width,NewDevDesc *dd) {
        DRAWSPEC;
        SET(RQUARTZ_FONT);
        char    text[2];
        text[0] = c;
        text[1] = '\0';
        CGFontRef font = CGContextGetFont(ctx);
        float aScale   = (gc->cex*gc->ps*xd->tscale)/CGFontGetUnitsPerEm(font);
        UniChar *buffer;
        CGGlyph   *glyphs;
        int Free = 0,len,i;
        *width = 0.0;
        CFStringRef str = prepareText(gc,dd,text,&buffer,&Free);
        len = CFStringGetLength(str);
        glyphs = malloc(sizeof(CGGlyph)*len);
        CGFontGetGlyphsForUnichars(font,buffer,glyphs,len);
        int      *advances = malloc(sizeof(int)*len);
        CGRect   *bboxes   = malloc(sizeof(CGRect)*len);
        CGFontGetGlyphAdvances(font,glyphs,len,advances);
        CGFontGetGlyphBBoxes(font,glyphs,len,bboxes);
        for(i=0;i<len;i++) *width += advances[i]*aScale;
        *ascent  = aScale*(bboxes[0].size.height + bboxes[0].origin.y);
        *descent = -aScale*bboxes[0].origin.y;
        free(bboxes);
        free(advances);
        free(glyphs);
        if(Free) free(buffer);
        CFRelease(str);
}

static Rboolean RQuartz_Locator(double *x,double *y,DEVDESC) {
        DEVSPEC;
        ctx = NULL;
		if(NULL == xd->locatePoint)
			return FALSE;
		return xd->locatePoint(xd,xd->userInfo,x,y);
}
