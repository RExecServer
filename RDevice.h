#import <Cocoa/Cocoa.h>
#include "RDeviceImpl.h"

@class RDevice;

@protocol RDeviceDelegate
- (void)willChangeDelegateForDevice:(RDevice*)aDevice;
- (void)didChangeDelegateForDevice:(RDevice*)aDevice;

- (void)didUpdateDevice:(RDevice*)aDevice;
- (void)didActivateDevice:(RDevice*)aDevice withNumber:(int)num;
- (void)didDeactivateDevice:(RDevice*)aDevice withNumber:(int)num;

- (void)willCloseDevice:(RDevice*)aDevice;

- (void)willBeginPageForDevice:(RDevice*)aDevice;
- (void)didEndPageForDevice:(RDevice*)aDevice;

@end

@interface RDevice : NSObject {
	QuartzDesc_t desc;
	NSString					*target;
	NSString					*display;
	id<RDeviceDelegate>          delegate;
}
+ (Class)deviceForDisplay:(NSString*)display;
+ (void)registerDevice:(Class)cls forDisplay:(NSString*)display;

- (id<RDeviceDelegate>)delegate;
- (void)setDelegate:(id<RDeviceDelegate>)delegate;

- (id)initWithDevice:(void*)dd size:(NSSize)size pointSize:(double)ps display:(NSString*)aDisplay target:(NSString*)aTarget 
	background:(int)bg antialias:(BOOL)antialias;
- (BOOL)canResize;
- (BOOL)canLocate;
- (BOOL)canDrawInView;

- (void)finishOpening;
- (void)flushDrawing;

- (NSSize)size;
- (void)setSize:(NSSize)newSize;

- (void)drawInRect:(NSRect)aRect;
- (void)redraw;

- (NSData*)deviceRepresentationOfType:(NSString*)aType;
- (NSString*)target;
- (NSString*)display;
- (void)close;

- (bycopy NSData*)snapshot;
- (void)setSnapshot:(NSData*)aData;

@end
