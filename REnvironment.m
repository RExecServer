#import "REnvironment.h"
#include <Rinternals.h>
#include <R_ext/Callbacks.h>

#undef error

@implementation REnvironment
+ (REnvironment*)globalEnv { 
	static REnvironment *globalEnv = nil;
	if(nil == R_GlobalEnv) return nil;
	if(nil == globalEnv) globalEnv = [[self objectWithSEXP:R_GlobalEnv preserve:NO] retain];
	return globalEnv;
}

- (NSArray*)names {
	NSMutableArray *arr = [[NSMutableArray alloc] init];
	SEXP ls;
	PROTECT(ls = lang2(install("ls"),(SEXP)obj_ptr));
	SET_TAG(CDR(ls),install("envir"));
	int err;
	SEXP res;
	PROTECT(res = R_tryEval(ls,R_GlobalEnv,&err));
	if(err == 0 && res != R_NilValue) {
		int i;
		for(i=0;i<length(res);i++) [arr addObject:[NSString stringWithUTF8String:CHAR(STRING_ELT(res,i))]];
	}
	UNPROTECT(2);
	return [arr autorelease];
}

- (RObject*)objectWithName:(NSString*)aName {
	SEXP var = findVarInFrame3((SEXP)obj_ptr,install([aName UTF8String]),TRUE);
	return (var == R_UnboundValue) ? nil : [RObject objectWithSEXP:var];
}


- (NSArray*)objectDescriptions {
	NSMutableArray *arr = [[self names] retain];
	int i;
	for(i=0;i<[arr count];i++) {
		NSString *name = [arr objectAtIndex:i];
		[arr replaceObjectAtIndex:i withObject:[[self objectWithName:name] describeWithName:name]];
	}
	return [arr autorelease];
}

- (BOOL)unserializeObject:(NSData*)aData toName:(NSString*)aName overwrite:(BOOL)shouldOverwrite {
	BOOL ret = NO;
	SEXP val;
	SEXP sym = install([aName UTF8String]);
	PROTECT(val = [aData unserialize]);
	SEXP check = findVarInFrame3((SEXP)obj_ptr,sym,TRUE);
	if(YES == shouldOverwrite || R_UnboundValue == check) {
			setVar(sym,val,(SEXP)obj_ptr);
			ret = YES;
	}
	UNPROTECT(1);
	return ret;
}

- (RServerConnection*)connectToServer:(NSString*)aServer host:(NSString*)aHost {
	return [RServerConnection serverConnectionToServer:aServer host:aHost environment:self];
}

@end
