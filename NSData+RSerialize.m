#import "NSData+RSerialize.h"

typedef struct nsdata_reader {
	int     index;
	NSData *data;
	char   *buffer;
} nsdata_reader_t;

int nsdata_in_char(R_inpstream_t stream) {
	nsdata_reader_t *d = stream->data;
	return d->buffer[d->index++];
}
void nsdata_in_bytes(R_inpstream_t stream,void*buffer,int size) {
	nsdata_reader_t *d = stream->data;
	if(size+d->index > [d->data length]) error("bad read length");
	memcpy(buffer,&(d->buffer[d->index]),size);
	d->index+=size;
}

@implementation NSData (RUnserialize)
+ (NSData*)dataWithSEXP:(SEXP)obj {
	NSMutableData *data = [[NSMutableData alloc] init];
	[data serialize:obj];
	return [data autorelease];
}
- (SEXP)unserialize {
	nsdata_reader_t me;
	me.index  = 0;
	me.data   = self;
	me.buffer = (char*)[self bytes];
	
	
	struct R_inpstream_st stream;
	
	R_InitInPStream(&stream,(R_pstream_data_t)&me,R_pstream_any_format,nsdata_in_char,nsdata_in_bytes,NULL,R_NilValue);
	return R_Unserialize(&stream);
}

@end

void nsdata_out_char(R_outpstream_t stream,int z) {
	char c = z;
	[(NSMutableData*)stream->data appendBytes:&c length:1];
}

void nsdata_out_bytes(R_outpstream_t stream,void*buffer,int size) {
	[(NSMutableData*)stream->data appendBytes:buffer length:size];
}

@implementation NSMutableData (RSerialize)

- (void)serialize:(SEXP)object {
	struct R_outpstream_st stream;
	R_InitOutPStream(&stream,(R_pstream_data_t)self,R_pstream_xdr_format,0,nsdata_out_char,nsdata_out_bytes,NULL,R_NilValue);
	R_Serialize(object,&stream);
}

@end
