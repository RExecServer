#import <Cocoa/Cocoa.h>
#import "RDevice.h"
#import "REnvironment.h"

@interface RInterpreter : NSObject {

	char     *readerPrompt;
	unsigned char *readerBuffer,*readerTemp;
	int       readerBufferLength,readerBufferUsed;
	int       readerAddToHistory;

	NSLock   *evalLock;

	BOOL      configured,suppressOutput,allowTerminal,vend,waiting;
	NSString *vendName;	
	long bufferSize;
	NSMutableAttributedString *buffer;
	NSDictionary *outputTag,*errorTag,*promptTag;
	
	NSMutableArray *deviceList;
	int   _argc;
	char **_argv;
	
	
	
	id delegate;
}
+ (RInterpreter*)sharedInterpreter;
- (BOOL)isConfigured;
- (void)run;

#pragma mark Configuration
- (void)setArgv:(char**)argv argc:(int)argc;

- (void)setDelegate:(id)delegate;
- (id)delegate;

- (void)setBufferSize:(long)bufferSize;
- (long)bufferSize;

- (void)setAllowTerminal:(BOOL)aBool;
- (BOOL)allowTerminal;

- (void)setVend:(BOOL)aBool;
- (BOOL)vend;

- (void)setHomePath:(NSString*)aPath;
- (NSString*)homePath;

- (void)setLocaleIdentifier:(NSString*)aLocale;
- (NSString*)localeIdentifier;


#pragma mark Interaction Protocol
- (NSString*)serverName;

- (void)awakeConsole;
- (void)evaluateInput:(NSString*)aString;
- (NSArray*)devices;
- (RDevice*)deviceCopyOfType:(NSString*)aType withTarget:(NSString*)aTarget;

#pragma mark Moving Data Between Servers


- (REnvironment*)environment;

@end
