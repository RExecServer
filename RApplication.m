#import "RApplication.h"
#include <CoreFoundation/CoreFoundation.h>

@implementation RApplication
- (void)sendEvent:(NSEvent*)theEvent {
	[super sendEvent:theEvent];
	if(NSApplicationDefined == [theEvent type]) {
		switch([theEvent subtype]) {
		case 1337:[self stop:self];break;
		}
	}
}

- (void)setServicesMenu:(NSMenu*)aMenu {
	//Check to see if there is already a connection. If not call [super], otherwise 
	//do nothing since this isn't going to go well anyway.
	
	NSString *str = [[NSString alloc] initWithFormat:@"%@.ServiceProvider",[[NSBundle mainBundle] bundleIdentifier]];
	CFMessagePortRef port = CFMessagePortCreateRemote(NULL,(CFStringRef)str);
	[str release];
	if(NULL == port) 
		[super setServicesMenu:aMenu];
	else {
		CFRelease(port);
	}
	
}

@end
