//
//  TerminalDelegate.m
//  RExecServer
//
//  Created by Byron Ellis on 7/3/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "TerminalDelegate.h"
#import "RInterpreter.h"
#import "RDevice.h"
#import "DeviceWindowController.h"
#import "WidgetWindowController.h"

#include <R_ext/eventloop.h>

void Terminal_ProcessEvents(void) {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	[NSApp postEvent:[NSEvent otherEventWithType:NSApplicationDefined 
									location:NSMakePoint(0,0)
									 modifierFlags:0
									 timestamp:0 
									 windowNumber:0 
									 context:nil 
									 subtype:1337 
									 data1:0 
									 data2:0] atStart:NO];
	[NSApp run];
	[pool release];
}

@implementation TerminalDelegate
- (id)init {
	if(nil == [super init]) return nil;
	
	readerFn = NULL;
	writerFn = NULL;
	flushFn  = NULL;
	
	readerCurMax = 0;
	readerBuffer = NULL;
	
	return self;
}

- (void)setReaderFunction:(R_READER_FN)fn {
	readerFn = fn;
}
- (void)setWriterFunction:(R_WRITER_FN)fn {
	writerFn = fn;
}
- (void)setFlushFunction:(R_FLUSH_FN)fn {
	flushFn = fn;
}

- (void)didOpenDevice:(id)aDevice forInterpreter:(id)anInterpreter {
	if(YES == [aDevice canDrawInView]) {
		DeviceWindowController *devCtrl = [[DeviceWindowController alloc] initWithWindowNibName:@"Device"];
		[devCtrl setDevice:aDevice];
		[[devCtrl window] makeKeyAndOrderFront:self];
	}
}

- (void)didCloseDevice:(id)aDevice forInterpreter:(id)anInterpreter { }


- (BOOL)shouldBufferOutputForInterpreter:(id)anInterpreter { return NO; }
- (void)appendString:(NSString*)outputString ofType:(int)aType forInterpreter:(id)anInterpreter {
	writerFn((char*)[outputString UTF8String],[outputString length]);
}

- (void)didFinishEvaluationForInterpreter:(id)anInterpreter {
}
- (void)didBeginEvaluationForInterpreter:(id)anInterpreter {
}
- (void)didBeginWaitingForInputWithMaximumLength:(int)bufferLength addToHistory:(BOOL)shouldAdd forInterpreter:(id)anInterpreter {
	if(readerCurMax < bufferLength) {
		if(NULL != readerBuffer) free(readerBuffer);
		readerBuffer = calloc(sizeof(unsigned char),bufferLength);
		readerCurMax = bufferLength;
	}
	R_PolledEvents = Terminal_ProcessEvents;
	R_wait_usec    = 10000;
	memset(readerBuffer,0,sizeof(unsigned char)*readerCurMax);
	readerUsed     = readerFn("",readerBuffer,bufferLength,shouldAdd == YES ? 1 : 0);
	[anInterpreter evaluateInput:[NSString stringWithUTF8String:(char*)readerBuffer]];
}
- (void)didGetInputForInterpreter:(id)anInterpreter {
}
- (void)flushOutputForInterpreter:(id)anInterpreter {
}
- (void)didCloseInterpreter:(id)anInterpreter {
}

- (void)openWidgetAtPath:(NSString*)aPath forInterpreter:(id)anInterpreter {
	NSBundle *bundle = [NSBundle bundleWithPath:aPath];
	NSWindow *window = [[NSWindow alloc] initWithContentRect:NSRect(0,0,0,0)
	 styleMask:NSBorderlessWindowMask backing:NSBackingStoreRetained defer:YES];
	
}

@end
