//
//  RApplicationDelegate.m
//  RExecServer
//
//  Created by Byron Ellis on 6/26/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "RApplicationDelegate.h"
#import "RInterpreter.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <Security/Authorization.h>
#include <Security/AuthorizationTags.h>

@implementation RApplicationDelegate

- (BOOL)applicationShouldOpenUntitledFile:(NSApplication*)sender { return NO; }
- (void)applicationWillFinishLaunching:(NSNotification*)aNotification {
	RInterpreter *ri = [RInterpreter sharedInterpreter];

	if(!(NULL != getenv("RNOTERM") && strncmp(getenv("RNOTERM"),"t",1)==0) && 
		((NULL != getenv("EMACS") && strncmp(getenv("EMACS"),"t",1)==0) || (NULL != getenv("TERM"))))
	   [ri setAllowTerminal:YES];
	if(NULL != getenv("RNOVEND"))
		[ri setVend:NO];
	
	/*
	struct stat sb;
	if(-1==lstat("/usr/bin/R-serve",&sb)) {
		switch(errno) {
			case ENOENT:{
				NSAlert *alert = [NSAlert alertWithMessageText:@"RExecServer can create a symbolic link named R-server in /usr/bin. Would you like to do this now?"
					defaultButton:@"Yes"
					alternateButton:@"No" 
					otherButton:nil informativeTextWithFormat:@""];
				if(NSAlertDefaultReturn == [alert runModal]) {
					
					AuthorizationRef auth;
					if(errAuthorizationSuccess == AuthorizationCreate(NULL,kAuthorizationEmptyEnvironment,kAuthorizationFlagDefaults,&auth)) {
						AuthorizationItem   item   = {kAuthorizationRightExecute,0,NULL,0};
						AuthorizationRights rights = {1,&item};
						if(errAuthorizationSuccess == AuthorizationCopyRights(auth,&rights,NULL,
							kAuthorizationFlagDefaults|kAuthorizationFlagInteractionAllowed|kAuthorizationFlagPreAuthorize|kAuthorizationFlagExtendRights
							,NULL)) {
								char *toolpath = "/bin/ln";
								char *toolargs[3];
								toolargs[0] = "-s"; 
								toolargs[1] = (char*)[[[NSBundle mainBundle] pathForResource:@"RExecServer" ofType:@"sh"] UTF8String];
								toolargs[2] = "/usr/bin/R-serve";
								OSStatus status;
								if(errAuthorizationSuccess != (status = AuthorizationExecuteWithPrivileges(auth,toolpath,kAuthorizationFlagDefaults,toolargs,NULL)))
									NSLog(@"Unable to execute tool [%d]: %s %s %s %s",status, toolpath,toolargs[0],toolargs[1],toolargs[2]);
						}
						AuthorizationFree(auth,kAuthorizationFlagDefaults);
					}

				
				
					symlink([[NSBundle mainBundle] executablePath],"/usr/bin/R-serve");
				}
				break;
			}
		}
	}
	*/
	   
}
- (void)applicationDidFinishLaunching:(NSNotification*)aNotification {
	//Process the remaining events and then bail out to initialize R.
	[NSApp postEvent:[NSEvent otherEventWithType:NSApplicationDefined 
									location:NSMakePoint(0,0)
									 modifierFlags:0
									 timestamp:0 
									 windowNumber:0 
									 context:nil 
									 subtype:1337 
									 data1:0 
									 data2:0] atStart:NO];	
}

@end
