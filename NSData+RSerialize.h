#import <Cocoa/Cocoa.h>
#include <Rinternals.h>

@interface NSData (RUnserialize)
+ (NSData*)dataWithSEXP:(SEXP)obj;
- (SEXP)unserialize;
@end

@interface NSMutableData (RSerialize)
- (void)serialize:(SEXP)obj;
@end
