#import <Cocoa/Cocoa.h>

//Protocol exported by the RInterpreter
@protocol RInterpreterProtocol

//Setting up the Console
- (void)setDelegate:(id)delegate;
- (id)delegate;

- (void)awakeConsole;

- (NSArray*)devices;


@end