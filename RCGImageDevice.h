#import <Cocoa/Cocoa.h>
#import "RDevice.h"

@interface RCGImageDevice : RDevice {
	CGContextRef   context;
	unsigned char *bitmap;
	size_t         bitmapSize;
}


@end
