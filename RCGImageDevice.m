#import "RCGImageDevice.h"

@implementation RCGImageDevice
+ (void)load {
	[RDevice registerDevice:self forDisplay:@"jpeg"];
	[RDevice registerDevice:self forDisplay:@"png"];
	[RDevice registerDevice:self forDisplay:@"tiff"];
	[RDevice registerDevice:self forDisplay:@""];
}

- (void)didResize {
	//Dispose of old context if needed
	if(context != NULL && bitmap != NULL) {
		CGContextRelease(context);
		free(bitmap);
		bitmapSize = 0;
	}
	
	NSSize size = [self size];
	size_t width    = 1*size.width;
	size_t height   = 1*size.height;
	size_t rowBytes = (width*8*4+7)/8;
	
	bitmapSize = rowBytes*height;
	bitmap = malloc(bitmapSize);
	memset(bitmap,0,bitmapSize);
	context = CGBitmapContextCreate(bitmap,width,height,8,rowBytes,CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB),kCGImageAlphaPremultipliedLast);
	CGContextTranslateCTM(context,0,1*size.height);
	CGContextScaleCTM(context,1.0,-1.0);
	CGContextSetShouldAntialias(context,RDevice_GetAntialias(desc) ? true : false);
	RDevice_ResetGDepth(desc);
}

- (void)finishOpening {
	RDevice_SetScale(desc,1.0);
	[self didResize];
}

- (void)drawInRect:(NSRect)rect {
	if(NULL != context) {
		CGImageRef image = CGBitmapContextCreateImage(context);
		CGContextDrawImage([[NSGraphicsContext currentContext] graphicsPort],CGRectMake(rect.origin.x,rect.origin.y,rect.size.width,rect.size.height),image);
		CGImageRelease(image);
	}
}

- (CGContextRef)context {
	return context;
}

- (BOOL)canDrawInView { return YES; }
- (BOOL)canResize { return YES; }

- (void)close {
	if(nil != [self display] && nil != [self target]) {
		NSData *data = [self deviceRepresentationOfType:[self display]];
		if(nil != data) {
			[data retain];
			[data writeToURL:[NSURL URLWithString:[self target]] atomically:YES];
			[data release];
		}
	}
	[super close];
}

- (NSData*)deviceRepresentationOfType:(NSString*)aType {
	if((nil == aType || [aType isEqual:@""])) {
		NSString *aDisplay = [self display];
		//If we don't have a display just return the raw bitmap
		if(nil == aDisplay || [aDisplay isEqual:@""]) 
			return [[[NSData alloc] initWithBytes:bitmap length:bitmapSize] autorelease];
		else aType = aDisplay;
	}
	if([aType isEqual:@"tiff"] || [aType isEqual:NSTIFFPboardType]) {
	}
	return nil;
}

//For easier access
- (NSData*)deviceRepresentation { return [self deviceRepresentationOfType:nil]; }


@end
