//
//  DeviceWindowController.m
//  RExecServer
//
//  Created by Byron Ellis on 6/29/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "DeviceWindowController.h"


@implementation DeviceWindowController

- (void)awakeFromNib {
}

- (void)setDevice:(RDevice*)aDevice {
	[[self window] setContentSize:[aDevice size]];
	[aDevice setDelegate:self];
}

- (void)willChangeDelegateForDevice:(RDevice*)aDevice {
	if(device != nil && aDevice == device) {
		[device release];
		device = nil;
	}
	[deviceView willChangeDelegateForDevice:aDevice];
}
- (void)didChangeDelegateForDevice:(RDevice*)aDevice {
	if(device != nil && aDevice != device) [self willChangeDelegateForDevice:device];
	device = [aDevice retain];
	[deviceView didChangeDelegateForDevice:aDevice];
}

- (void)didUpdateDevice:(RDevice*)aDevice {
	[deviceView didUpdateDevice:aDevice];
}

- (void)didActivateDevice:(RDevice*)aDevice withNumber:(int)num {
	[[self window] setTitle:[NSString stringWithFormat:@"Quartz Device %d - Active",num]];
}
- (void)didDeactivateDevice:(RDevice*)aDevice withNumber:(int)num {
	[[self window] setTitle:[NSString stringWithFormat:@"Quartz Device %d - Inactive",num]];
}

- (void)willCloseDevice:(RDevice*)aDevice {
	if(aDevice == device) {
		[device release];
		device = nil;
		[[self window] performClose:self];
	}
}

- (void)didEndPageForDevice:(RDevice*)aDevice {
}

- (void)willBeginPageForDevice:(RDevice*)aDevice {
}

@end
