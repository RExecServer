#import <Cocoa/Cocoa.h>
#import "RInterpreter.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/resource.h>

int main(int argc, char *argv[])
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	[[RInterpreter sharedInterpreter] setArgv:argv argc:argc];
	NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
	Class         Main = NSClassFromString([info objectForKey:@"NSPrincipalClass"]);
	if(nil == Main) {
		NSLog(@"Unable to locate principal class: %@",[info objectForKey:@"NSPrincipalClass"]);
		[pool release];
		return 1;
	}
	//Let the application run its setup procedures
	[Main sharedApplication];
	[NSBundle loadNibNamed:[info objectForKey:@"NSMainNibFile"] owner:NSApp];
	[NSApp run];
	[pool release];
	//Turn primary control over to the R event loop.
	[[RInterpreter sharedInterpreter] run];
	//If we get here then we should shut down the application.
	return 0;
}
