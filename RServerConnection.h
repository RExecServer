#import <Cocoa/Cocoa.h>

@class REnvironment;

@interface RServerConnection : NSObject {
	REnvironment *env;
	id            server;
}
+ (RServerConnection*)serverConnectionToServer:(NSString*)aServer host:(NSString*)hostName environment:(REnvironment*)anEnv;
- (id)initWithConnectionToServer:(NSString*)aServer host:(NSString*)hostName environment:(REnvironment*)aEnv;
- (BOOL)synchronizePackages;
- (BOOL)copyObject:(NSString*)aFrom toName:(NSString*)aTo;

@end
