#import <Cocoa/Cocoa.h>


@interface RObject : NSObject {
	void* obj_ptr;
	BOOL  preserved;
}
+ (id)objectWithSEXP:(void*)aSexp preserve:(BOOL)shouldPreserve;
+ (id)objectWithSEXP:(void*)aSexp;

- (id)initWithSEXP:(void*)aSexp preserve:(BOOL)shouldPreserve;
- (id)initWithSEXP:(void*)aSexp;

- (int)length;
- (NSArray*)classList;
- (BOOL)isMatrix;
- (NSArray*)dimensions;

- (NSData*)serialize;
- (id)coerce;

- (NSDictionary*)describeWithName:(NSString*)aName;
- (NSDictionary*)descibe;

@end
