#import "RObject.h"
#import "NSData+RSerialize.h"

#include <Rinternals.h>
#undef error
#undef length

@implementation RObject
+ (id)objectWithSEXP:(void*)aSexp preserve:(BOOL)shouldPreserve { return [[[self alloc] initWithSEXP:aSexp preserve:shouldPreserve] autorelease]; }
+ (id)objectWithSEXP:(void*)aSexp { return [[[self alloc] initWithSEXP:aSexp] autorelease]; }

- (id)initWithSEXP:(void*)aSexp preserve:(BOOL)shouldPreserve {
	if(nil == [super init]) return nil;
	if(YES == shouldPreserve) R_PreserveObject((SEXP)aSexp);
	obj_ptr = aSexp;
	preserved = shouldPreserve;
	return self;
}
- (id)initWithSEXP:(void*)aSexp { return [self initWithSEXP:aSexp preserve:YES]; }

- (id)initWithData:(NSData*)data {
	return [self initWithSEXP:[data unserialize]];
}

- (void)dealloc {
	if(obj_ptr != NULL && YES == preserved) R_ReleaseObject((SEXP)obj_ptr);
	[super dealloc];
}


- (NSData*)serialize { return [NSData dataWithSEXP:(SEXP)obj_ptr]; }


- (id)coerce {
	return nil;
}

- (int)length { return Rf_length((SEXP)obj_ptr); }

- (NSArray*)dimensions {
	NSMutableArray *arr = [[NSMutableArray alloc] init];
	SEXP e,dims;
	PROTECT(e = lang2(install("dim"),(SEXP)obj_ptr));
	int  err,i;
	PROTECT(dims = R_tryEval(e,R_GlobalEnv,&err));
	if(!err && R_NilValue != dims) {
		for(i=0;i<Rf_length(dims);i++) [arr addObject:[NSNumber numberWithInt:INTEGER(dims)[i]]];
	}
	UNPROTECT(2);
	return [arr autorelease];
}

- (BOOL)isMatrix {
	SEXP e;
	PROTECT(e = lang2(install("dim"),(SEXP)obj_ptr));
	int  err;
	BOOL isMatrix;
	isMatrix = (R_NilValue == R_tryEval(e,R_GlobalEnv,&err)) ? NO : YES;
	if(err) isMatrix = NO;
	UNPROTECT(1);
	return isMatrix;
}
- (NSArray*)classList {
	NSMutableArray *arr = [[NSMutableArray alloc] init];
	SEXP klass = getAttrib((SEXP)obj_ptr,R_ClassSymbol);
	if(STRSXP == TYPEOF(klass)) {
		int i;
		for(i=0;i<Rf_length(klass);i++) [arr addObject:[NSString stringWithUTF8String:CHAR(STRING_ELT(klass,i))]];
	} else {
		if(R_NilValue != getAttrib((SEXP)obj_ptr,R_DimSymbol)) [arr addObject:@"matrix"];
		switch(TYPEOF(klass)) {
			case INTSXP:[arr addObject:@"integer"];break;
			case REALSXP:[arr addObject:@"numeric"];break;
			case VECSXP:[arr addObject:@"list"];break;
		}
	}
	return [arr autorelease];
}

- (NSDictionary*)describeWithName:(NSString*)aName {
	NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
		[self classList],@"Class",[NSNumber numberWithInt:[self length]],@"Length",nil];
	if(nil != aName) [dict setObject:aName forKey:@"Name"];
	if(YES == [self isMatrix]) [dict setObject:[self dimensions] forKey:@"Dimensions"];
	return [dict autorelease];
}

- (NSDictionary*)describe { return [self describeWithName:nil]; }

@end
