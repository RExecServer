#ifndef _H_RDEVICEIMPL_
#define _H_RDEVICEIMPL_

#include <ApplicationServices/ApplicationServices.h>


typedef void* QuartzDesc_t;

QuartzDesc_t RDevice_Create(
	void*dd,double scale,double ps,double width,double height,int bg,int aa,
	CGContextRef (*getCGContext)(QuartzDesc_t dev,void*userInfo), //Get the context for this device
	int          (*locatePoint)(QuartzDesc_t dev,void*userInfo,double*x,double*y),
	void         (*close)(QuartzDesc_t dev,void*userInfo),
	void         (*newPage)(QuartzDesc_t dev,void*userInfo),
	void         (*activate)(QuartzDesc_t dev,void*userInfo,int devNum),
	void         (*deactivate)(QuartzDesc_t dev,void*userInfo,int devNum),	
	void*userInfo);

double RDevice_GetWidth(QuartzDesc_t desc);
void   RDevice_SetWidth(QuartzDesc_t desc,double width);

double RDevice_GetHeight(QuartzDesc_t desc);
void   RDevice_SetHeight(QuartzDesc_t desc,double height);


void RDevice_SetScale(QuartzDesc_t desc,double scale);
double RDevice_GetScale(QuartzDesc_t desc);

void RDevice_SetTextScale(QuartzDesc_t desc,double scale);
double RDevice_GetTextScale(QuartzDesc_t desc);

void RDevice_SetPointSize(QuartzDesc_t desc,double ps);
double RDevice_GetPointSize(QuartzDesc_t desc);

int  RDevice_GetDirty(QuartzDesc_t desc);
void RDevice_SetDirty(QuartzDesc_t desc,int dirty);

void  RDevice_ReplayDisplayList(QuartzDesc_t desc);
void* RDevice_GetSnapshot(QuartzDesc_t desc);
void  RDevice_RestoreSnapshot(QuartzDesc_t desc,void* snapshot);

int  RDevice_GetGDepth(QuartzDesc_t desc);
void RDevice_ResetGDepth(QuartzDesc_t desc);

int RDevice_GetAntialias(QuartzDesc_t desc);

#endif
