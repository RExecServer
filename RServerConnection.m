//
//  RServerConnection.m
//  RExecServer
//
//  Created by Byron Ellis on 7/9/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "RServerConnection.h"
#import "REnvironment.h"

@implementation RServerConnection
+ (RServerConnection*)serverConnectionToServer:(NSString*)aServer host:(NSString*)hostName environment:(REnvironment*)anEnv {
	return [[[self alloc] initWithConnectionToServer:aServer host:hostName environment:anEnv] autorelease];
}
- (id)initWithConnectionToServer:(NSString*)aServer host:(NSString*)hostName environment:(REnvironment*)aEnv {
	if(nil == [super init]) return nil;
	server = [NSConnection rootProxyForConnectionWithRegisteredName:aServer host:hostName];
	if(nil == server) {
		[self release];
		return nil;
	}
	env = [aEnv retain];
	return self;
}
- (void)dealloc {
	if(nil != env) [env release];
	[super dealloc];
}

- (BOOL)synchronizePackages { return NO; }
- (BOOL)copyObject:(NSString*)aFrom toName:(NSString*)aTo {
	NSData *data = [[(REnvironment*)[server environment] objectWithName:aFrom] serialize];
	return [env unserializeObject:data toName:aTo overwrite:YES];
}

@end
