//
//  RFunction.h
//  RExecServer
//
//  Created by Byron Ellis on 7/6/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "RObject.h"

@interface RFunction : RObject {

}

+ (RFunction*)functionWithName:(NSString*)aFun;

@end
