//
//  DeviceView.h
//  RExecServer
//
//  Created by Byron Ellis on 6/29/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "RDevice.h"

@interface DeviceView : NSView<RDeviceDelegate> {
	RDevice *device;
}

@end
