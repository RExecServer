//
//  RFunction.m
//  RExecServer
//
//  Created by Byron Ellis on 7/6/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "RFunction.h"
#include <Rinternals.h>

@implementation RFunction

+ (RFunction*)functionWithName:(NSString*)aFun {
	
}

- (SEXP)_formals {
	SEXP e,ret;
	int  err;
	PROTECT(e = lang2(install("formals"),(SEXP)obj_ptr));
	ret = R_tryEval(e,R_GlobalEnv,&err);
	if(err == 0) {
		UNPROTECT(1);
		return ret;
	} else {
		UNPROTECT(1);
		return R_NilValue;
	}
}

- (NSArray*)arguments {
}

- (NSDictionary*)defaults {
}


@end
