#import <Cocoa/Cocoa.h>
#import "RObject.h"

@interface RDataPortManager : NSObject {
	NSString       *portName;
	NSMutableArray *subscribers;
}
- (void)setName:(NSString*)aName;
- (void)addSubscriber:(id)subs;
- (void)removeSubscriber:(id)subs;

- (void)setValue:(id)value;

@end
