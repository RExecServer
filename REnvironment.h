#import <Cocoa/Cocoa.h>
#import "RObject.h"
#import "RServerConnection.h"
#import "NSData+RSerialize.h"

@interface REnvironment : RObject {
}
+ (REnvironment*)globalEnv;

- (RServerConnection*)connectToServer:(NSString*)aServer host:(NSString*)aHost;

- (NSArray*)names;
- (RObject*)objectWithName:(NSString*)aName;
- (NSArray*)objectDescriptions;

- (BOOL)unserializeObject:(NSData*)obj toName:(NSString*)aName overwrite:(BOOL)shouldOverwrite;

@end
