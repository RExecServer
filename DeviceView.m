//
//  DeviceView.m
//  RExecServer
//
//  Created by Byron Ellis on 6/29/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "DeviceView.h"


@implementation DeviceView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)rect {
	if(device != nil) {
		[device drawInRect:[self bounds]];
	}
}

- (void)setFrame:(NSRect)frame {
	[super setFrame:frame];
	if(NO == [self inLiveResize]) {
		[device setSize:frame.size];
		[device redraw];
		[self display];
	}
}

- (void)viewDidEndLiveResize {
	[device setSize:[self bounds].size];
	[device redraw];
	[self display];
}


- (void)willChangeDelegateForDevice:(RDevice*)aDevice {
	if(device != nil && aDevice == device) {
		[device release];
		device = nil;
	}
}
- (void)didChangeDelegateForDevice:(RDevice*)aDevice {
	if(device != nil && aDevice != device) [self willChangeDelegateForDevice:device];
	device = [aDevice retain];
}

- (void)didUpdateDevice:(RDevice*)aDevice {
	if(aDevice == device) [self display];
}

- (void)didActivateDevice:(RDevice*)aDevice withNumber:(int)num {

}
- (void)didDeactivateDevice:(RDevice*)aDevice withNumber:(int)num {

}

- (void)willCloseDevice:(RDevice*)aDevice { }

- (void)didEndPageForDevice:(RDevice*)aDevice {
}
- (void)willBeginPageForDevice:(RDevice*)aDevice {
}

@end
