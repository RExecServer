#import <Cocoa/Cocoa.h>

typedef int  (*R_READER_FN)(char *,unsigned char*,int,int);
typedef void (*R_WRITER_FN)(char*,int);
typedef void (*R_FLUSH_FN)();

@interface TerminalDelegate : NSObject {
	//Reader thread stuff
	int            readerCurMax;
	unsigned char* readerBuffer;
	int            readerMaxLen,readerUsed;
	BOOL           readerHistory;
	
	R_READER_FN readerFn;
	R_WRITER_FN writerFn;
	R_FLUSH_FN  flushFn;
}

- (id)init;

- (void)setReaderFunction:(R_READER_FN)fn;
- (void)setWriterFunction:(R_WRITER_FN)fn;
- (void)setFlushFunction:(R_FLUSH_FN)fn;


//Device open/closing delegate methods
- (void)didOpenDevice:(id)aDevice forInterpreter:(id)anInterpreter;
- (void)didCloseDevice:(id)aDevice forInterpreter:(id)anInterpreter;

//Interpreter evaluation
- (void)didFinishEvaluationForInterpreter:(id)anInterpreter;
- (void)didBeginEvaluationForInterpreter:(id)anInterpreter;
- (void)didBeginWaitingForInputWithMaximumLength:(int)bufferLength addToHistory:(BOOL)shouldAdd forInterpreter:(id)anInterpreter;
- (void)didGetInputForInterpreter:(id)anInterpreter;
- (void)didCloseInterpreter:(id)anInterpreter;

- (BOOL)shouldBufferOutputForInterpreter:(id)anInterpreter;
- (void)appendString:(NSString*)outputString ofType:(int)aType forInterpreter:(id)anInterpreter;
- (void)flushOutputForInterpreter:(id)anInterpreter;

- (void)openWidgetAtPath:(NSString*)aPath forInterpreter:(id)anInterpreter;

@end
