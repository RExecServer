#import "RInterpreter.h"
#import "RDevice.h"
#import "DeviceWindowController.h"
#import "NSData+RSerialize.h"
#import "TerminalDelegate.h"

#define R_INTERFACE_PTRS 1
#define CSTACK_DEFNS     1

#include <Rinternals.h>
#include <Rinterface.h>
#include <R_ext/Utils.h>
#include <Rgraphics.h>
#include <R_ext/GraphicsDevice.h>
#include <R_ext/eventloop.h>
#include <R_ext/Rdynload.h>
#include <Rversion.h>

#include <pthread.h>
#include <signal.h>


#undef Boolean
#undef error

@interface RInterpreter (Private)
- (void)configure;
- (void)readyToEvaluate;
- (Rboolean)openDevice:(NewDevDesc *)dev withDisplay:(char *)display width:(double)width height:(double)height
	pointsize:(double)ps family:(char*)family antialias:(Rboolean)antialias autorefresh:(Rboolean)autorefreash 
	quartzpos:(int)quartzpos background:(int)bg;
- (void)registerInterface;
@end

@implementation RInterpreter

+ (RInterpreter*)sharedInterpreter {
	static RInterpreter *interp = nil;
	if(nil == interp) interp = [[RInterpreter alloc] init];
	return interp;
}

- (id)init {
	if(nil == [super init]) return nil;
	
	//Some configuration defaults
	bufferSize           = 2048;
	buffer               = [[NSMutableAttributedString alloc] init];


	suppressOutput       = NO;
	allowTerminal        = NO;
	vend                 = YES;
	waiting              = YES;
	delegate             = nil;

	deviceList = [[NSMutableArray alloc] init];
	evalLock   = [[NSLock alloc] init];
	
	_argc = 0;
	_argv = NULL;

	outputTag = [[NSDictionary alloc] initWithObjectsAndKeys:@"ROutput",@"RTextType",nil];
	promptTag = [[NSDictionary alloc] initWithObjectsAndKeys:@"RInput",@"RTextType",nil];
	errorTag  = [[NSDictionary alloc] initWithObjectsAndKeys:@"RError",@"RTextType",nil];

	//Set some environment variables to their defaults if they are not presently set.
	setenv("R_HOME",[[[NSBundle bundleWithIdentifier:@"org.r-project.R-framework"] resourcePath] UTF8String],0);
	setenv("LANG",[[NSString stringWithFormat:@"%@.UTF-8",[[NSLocale currentLocale] localeIdentifier]] UTF8String],0);
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeInterpreter:) name:NSApplicationWillTerminateNotification object:nil];
	
	return self;
}

- (void)closeInterpreter:(NSNotification*)aNotify {
	if(nil != delegate) [delegate didCloseInterpreter:self];
}

- (BOOL)isConfigured { return configured; }

- (void)setArgv:(char**)argv argc:(int)argc {
	_argc = argc;
	_argv = argv;
}

- (id)delegate { return delegate; }
- (void)setDelegate:(id)aDelegate { delegate = aDelegate; }

- (long)bufferSize   { return bufferSize; }
- (void)setBufferSize:(long)aSize {
	bufferSize = aSize;
}
- (BOOL)allowTerminal { return allowTerminal; }
- (void)setAllowTerminal:(BOOL)aBool { allowTerminal = aBool; }

- (BOOL)vend { return vend; }
- (void)setVend:(BOOL)aBool { vend = aBool; }

- (NSString*)homePath { return (NULL == getenv("R_HOME")) ? nil : [NSString stringWithUTF8String:getenv("R_HOME")]; }
- (void)setHomePath:(NSString*)aPath {
	setenv("R_HOME",[aPath UTF8String],1);
}

- (NSString*)localeIdentifier { return (NULL == getenv("LANG")) ? nil : [NSString stringWithUTF8String:getenv("LANG")]; }
- (void)setLocaleIdentifier:(NSString*)aLocale {
	setenv("LANG",[aLocale UTF8String],1);
}

- (void)_run {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	[evalLock lock];
	if(nil != delegate) [delegate didBeginEvaluationForInterpreter:self];
	setup_Rmainloop();
	[self registerInterface];
	[pool release];
	run_Rmainloop();
}

- (void)run {
	if(NO == [self isConfigured]) [self configure];
	if(YES == [self vend]) {
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		if(NULL != getenv("RVENDNAME")) {
			vendName = [[NSString alloc] initWithUTF8String:getenv("RVENDNAME")];
			NSLog(@"Vending with specified name: %@",vendName);
			NSConnection *conn = [NSConnection defaultConnection];
			[conn setRootObject:self];
			if(NO == [conn registerName:vendName]) {
				NSLog(@"Unable to register server as %@");
				[vendName release];
				vendName = nil;
			}
		} else {
			int vend_num = 0;
			NSConnection *conn = [NSConnection defaultConnection];
			[conn setRootObject:self];
			while(vend_num < 17) {
				vendName = [[NSString alloc] initWithFormat:@"R Execution Server %d",++vend_num];
				if(YES == [conn registerName:vendName]) {
					break;
				} else
					[vendName release];
			}
			if(vend_num == 17) {
				NSLog(@"Unable to register server. Presently, a maximum of 16 local execution servers are allowed per machine");
				vendName = nil;
			}
		}
		[pool release];
	}
	//If we don't have a delegate yet, enter the event loop. The delegate (when it connects)
	//should post a stop message via awakeConsole.
	if(nil == delegate) {
		NSLog(@"Waiting for a delegate");
		waiting = YES;
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		[[NSDistributedNotificationCenter defaultCenter] postNotificationName:@"RExecServerStartedVending" object:vendName];
		[NSApp run];
		[pool release];
	}
	waiting = NO;
	[self _run];
}


- (void)awakeConsole {
	if(YES == waiting) {
		NSLog(@"Awake Console");
		//Post a stop event so that we fall out of the wait loop at the 
		[NSApp postEvent:[NSEvent otherEventWithType:NSApplicationDefined 
										location:NSMakePoint(0,0)
										modifierFlags:0
										timestamp:0 
										windowNumber:0 
										context:nil 
										subtype:1337 
										data1:0 
										data2:0] atStart:NO];	
	}
}

- (void)evaluateInput:(NSString*)aString {
	readerBufferUsed = [aString length];
	if(readerBufferUsed > readerBufferLength) readerBufferUsed = readerBufferLength;
	memcpy(readerBuffer,[aString UTF8String],sizeof(unsigned char)*readerBufferUsed);
	[self readyToEvaluate];
}

- (NSArray*)devices { return deviceList; }

- (NSString*)serverName { return vendName; }
- (REnvironment*)environment { return [REnvironment globalEnv]; }

- (NSData*)serializeObjectWithName:(NSString*)anName error:(NSError**)anError {
	const char *name = [anName UTF8String];
	SEXP  obj  = findVar(install(name),R_GlobalEnv);
	if(obj != R_UnboundValue) {
		PROTECT(obj);
		NSMutableData *data = [[NSMutableData alloc] init];
		[data serialize:obj];
		UNPROTECT(1);
		return [data autorelease];
	} else { 
		return nil;
	}
}
- (BOOL)deserializeObject:(NSData*)anObject withName:(NSString*)aName replace:(BOOL)shouldReplace error:(NSError**)error {
	error = nil;
	if(anObject == nil) {
		*error = [NSError errorWithDomain:@"org.r-project.RExecServer" code:1 userInfo:nil];
		return NO;
	}
	
	
	const char *name = [aName UTF8String];
	SEXP  cur  = findVar(install(name),R_GlobalEnv);
	if(cur != R_UnboundValue && NO == shouldReplace) {
		*error = [NSError errorWithDomain:@"org.r-project.RExecServer" code:1 userInfo:nil];
		return NO;
	}
	Rf_setVar(install(name),[anObject unserialize],R_GlobalEnv);
	return YES;
}

- (void)copyObjectWithName:(NSString*)aName toServer:(NSString*)aServer error:(NSError**)error {
	id theProxy = [[NSConnection rootProxyForConnectionWithRegisteredName:aServer host:nil] retain];
	*error = nil;
	NSData *serialized = [self serializeObjectWithName:aName error:error];
	if(nil == error) 
		[theProxy deserializeObject:serialized withName:aName replace:YES error:error];
	[theProxy release];
}

- (RDevice*)deviceCopyOfType:(NSString*)aType withTarget:(NSString*)aTarget {
	return nil;
}


@end


#pragma mark Function Prototypes
void RInterp_Suicide(char*);
void RInterp_ShowMessage(char*);
void RInterp_FlushConsole();
void RInterp_WritePrompt(char*);
int  RInterp_ReadConsole(char*,unsigned char*,int,int);
void RInterp_ResetConsole();
void RInterp_WriteConsole(char*,int);
void RInterp_ClearerrConsole();
void RInterp_Busy();
void RInterp_CleanUp(SA_TYPE,int,int);
int  RInterp_ShowFiles(int,char**,char**,char*,Rboolean,char*);
int  RInterp_ChooseFile(int,char*,int);
int  RInterp_EditFile(char*);
void RInterp_System(char*);
void RInterp_ProcessEvents();
Rboolean RInterp_Device(NewDevDesc*,char*,double,double,double,char*,Rboolean,Rboolean,int,int);
void RInterp_DeviceParams(double*,double*,double*,char*,Rboolean*,Rboolean*,int*);
int  RInterp_CustomPrint(char *,SEXP);
SEXP RInterp_do_selectlist(SEXP,SEXP,SEXP,SEXP);



extern void     (*ptr_R_ProcessEvents)(void);
extern void     (*ptr_CocoaSystem)(char*);
extern Rboolean (*ptr_CocoaInnerQuartzDevice)(NewDevDesc*,char*,double,double,double,char*,Rboolean,Rboolean,int,int);
extern void     (*ptr_CocoaGetQuartzParameters)(double*,double*,double*,char*,Rboolean*,Rboolean*,int*);
extern int      (*ptr_Raqua_CustomPrint)(char *, SEXP);

extern DL_FUNC ptr_do_wsbrowser,
    ptr_do_dataentry, ptr_do_browsepkgs, ptr_do_datamanger,
    ptr_do_packagemanger, ptr_do_flushconsole, ptr_do_hsbrowser,
    ptr_do_selectlist;
	


extern void Rstd_WriteConsole(char*,int);

@implementation RInterpreter (Private)

- (void)_dummy { }

- (void)configure {
	char *argv_orig[] = {"R","--gui=cocoa","--no-save","--no-restore-data"};
	char **argv;
	int   argc = 0;
	
	if(_argc > 0) {
		int i,j,has_gui=0,has_g=0;
		
		argc=_argc;
		for(i=1;i<_argc;i++) {
			if(strncmp(_argv[i],"-g",2)==0) 
				has_g = 1; 
			else if(strncmp(_argv[i],"--gui",5)==0)
				has_gui = 1;
		}
		if(has_g || has_gui)
			printf("warning: Execution server will ignore GUI settings.\n");
		if(has_g) argc -= 1; else if(!has_g && !has_gui) argc++;
		argv = malloc(sizeof(char*)*argc);
		
		j=0;
		for(i=0;i<_argc;i++) {
			if(strncmp(_argv[i],"-g",2)==0) {
				i++;
			} else if(strncmp(_argv[i],"--gui",5)==0) {
			} else {
				argv[j++] = _argv[i];
			}
		}
		argv[j++] = "--gui=cocoa";
	} else {
		argc = 4;
		argv = argv_orig;
	}
	Rf_initialize_R(argc,argv);
	//R_GUIType = "RExecServer";	//We are NOT the Aqua GUI, but not being the aqua gui is even more annoying.
	if(YES == allowTerminal) {
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		TerminalDelegate *tempDel = [[TerminalDelegate alloc] init];
		[tempDel setReaderFunction:ptr_R_ReadConsole];
		[tempDel setWriterFunction:Rstd_WriteConsole];
		[tempDel setFlushFunction:ptr_R_FlushConsole];
		[self setDelegate:tempDel];
		[pool release];
	}
	R_Outputfile  = NULL;
	R_Consolefile = NULL;

#ifdef R_USING_TRAMPOLINE
//On systems supporting libffi we can generate a direct trampoline
//function that dispatches to the R level. This is coming later.
#else
    ptr_R_Suicide                = RInterp_Suicide;
    ptr_R_ShowMessage            = RInterp_ShowMessage;
    ptr_R_ReadConsole            = RInterp_ReadConsole;
    ptr_R_WriteConsole           = RInterp_WriteConsole;
	ptr_R_WriteConsoleEx         = NULL;
    ptr_R_ShowFiles              = RInterp_ShowFiles;
    ptr_R_EditFile               = RInterp_EditFile;
    ptr_R_ChooseFile             = RInterp_ChooseFile;
    ptr_Raqua_CustomPrint        = RInterp_CustomPrint;
	ptr_R_ProcessEvents          = RInterp_ProcessEvents;
    ptr_CocoaInnerQuartzDevice   = RInterp_Device;
    ptr_CocoaGetQuartzParameters = RInterp_DeviceParams;
    ptr_CocoaSystem              = RInterp_System;
#endif
	
	//Put ourselves into a multithreaded state
	//[NSThread detachNewThreadSelector:@selector(_dummy) toTarget:self withObject:nil];
}

- (void)writeConsole:(char*)output length:(int)aLength {
	char c = output[aLength];
	output[aLength] = '\0';
	if(nil != delegate) [delegate appendString:[NSString stringWithUTF8String:output] ofType:0 forInterpreter:self];
	output[aLength] = c;
}

- (int)readConsoleWithPrompt:(char*)aPrompt buffer:(unsigned char*)aBuffer length:(int)aLength history:(int)useHistory {
	//Put those there for something that wants to write a string
	readerPrompt = aPrompt;
	readerBuffer = aBuffer;
	readerBufferLength = aLength;
	readerAddToHistory = useHistory;
	
	memset(readerBuffer,0,sizeof(unsigned char)*aLength);
	
	//Flush any drawing that may have happened since the last time we were here.
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	if(nil != delegate) [delegate didFinishEvaluationForInterpreter:self];
	NSEnumerator *e = [deviceList objectEnumerator];
	RDevice *dev;
	while((dev = (RDevice*)[e nextObject]) != nil) {
		[dev flushDrawing];
	}
	[pool release];
	[evalLock unlock];
	pool = [[NSAutoreleasePool alloc] init];
	if(nil != delegate) [delegate appendString:[NSString stringWithUTF8String:aPrompt] ofType:2 forInterpreter:self];
	if(nil != delegate) [delegate didBeginWaitingForInputWithMaximumLength:readerBufferLength addToHistory:(useHistory == 1) ? YES : NO forInterpreter:self];
	[NSApp run];
	if(nil != delegate) [delegate didGetInputForInterpreter:self];
	[evalLock lock];
	if(nil != delegate) [delegate didBeginEvaluationForInterpreter:self];
	[pool release];
	return readerBufferUsed;
}

- (void)removeDevice:(NSNotification*)aNotify {
	if(nil != delegate) [delegate didCloseDevice:[aNotify object] forInterpreter:self];
	[deviceList removeObject:[aNotify object]];
}
	
- (Rboolean)openDevice:(NewDevDesc *)dev withDisplay:(char *)display width:(double)width height:(double)height
	pointsize:(double)ps family:(char*)family antialias:(Rboolean)antialias autorefresh:(Rboolean)autorefreash 
	quartzpos:(int)quartzpos background:(int)bg {
	NSString *aDisplay = [NSString stringWithUTF8String:display];
	NSArray  *bits     = [[aDisplay componentsSeparatedByString:@":"] retain];
	Class deviceClass = nil;
	deviceClass = [RDevice deviceForDisplay:([bits count] < 2) ? @"" : [bits objectAtIndex:1]];

	//Can't create device.
	if(nil == deviceClass) return 0;
	RDevice *rd = [[deviceClass alloc] initWithDevice:dev size:NSMakeSize(width,height) pointSize:ps 
		display:[bits objectAtIndex:0]
		target:[bits count] < 2 ? nil : [bits objectAtIndex:1] background:bg antialias:antialias];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeDevice:) name:@"RDeviceClosed" object:rd];
	[deviceList addObject:rd];
	if(nil != delegate) [delegate didOpenDevice:rd forInterpreter:self];
	[rd finishOpening];
	[rd release];
	return TRUE;
}

- (void)flushConsole {
}

- (void)showMessage:(char*)aMsg {
	[self writeConsole:aMsg length:strlen(aMsg)];
}

- (void)suicide:(char*)aMsg {
	[self writeConsole:aMsg length:strlen(aMsg)];
}

- (void)resetConsole {
}

- (void)clearErrorConsole {
}

- (void)busy {
}

- (void)system:(char*)cmd {
	system(cmd);
}

- (int)editFile:(char*)aFileName {
	if(YES == allowTerminal) {
		NSLog(@"Editing file: %s",aFileName);
		return 0;
	} else {
		return 0;
	}
}

- (int)showFiles:(int)nfiles file:(char**)file headers:(char**)headers wtitle:(char*)wtitle del:(Rboolean)del pager:(char*)pager {
	return 0;
}

- (int)customPrintType:(char*)aType list:(SEXP)aList {
	NSLog(@"This is hosed for right now. Might need support from R itself.");
	return 0;
}

- (SEXP)selectListWithCall:(SEXP)call op:(SEXP)op args:(SEXP)args rho:(SEXP)rho {
	return R_NilValue;
}

- (void)readyToEvaluate {
	[NSApp postEvent:[NSEvent otherEventWithType:NSApplicationDefined 
									location:NSMakePoint(0,0)
									 modifierFlags:0
									 timestamp:0 
									 windowNumber:0 
									 context:nil 
									 subtype:1337 
									 data1:0 
									 data2:0] atStart:NO];	
}

SEXP RES_ServerName() {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	const char *str = [[[RInterpreter sharedInterpreter] serverName] UTF8String];
	SEXP ret;
	PROTECT(ret = allocVector(STRSXP,1));
	SET_STRING_ELT(ret,0,mkChar(str));
	UNPROTECT(1);
	[pool release];
	return ret;
}

SEXP RES_CopyObject(SEXP name,SEXP server) {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	NSString *aStr = [[NSString alloc] initWithUTF8String:CHAR(STRING_ELT(name,0))];
	NSString *bStr = [[NSString alloc] initWithUTF8String:CHAR(STRING_ELT(server,0))];
	NSError *error = nil;
	[[RInterpreter sharedInterpreter] copyObjectWithName:aStr toServer:bStr error:&error];
	[aStr release];
	[bStr release];
	[pool release];
	if(nil != error)
		Rf_error("Problem copying object");
	return R_NilValue;
}

static const R_CallMethodDef R_CallDef[] = {
	{"RES_ServerName",(DL_FUNC)RES_ServerName,0},
	{"RES_CopyObject",(DL_FUNC)RES_CopyObject,2},
	NULL
};

extern void
R_addCallRoutine(DllInfo *info, const R_CallMethodDef * const croutine,void*sym);

- (void)registerInterface {
	R_registerRoutines(R_getEmbeddingDllInfo(),NULL,R_CallDef,NULL,NULL);
}

@end

#ifdef R_USING_TRAMPOLINE
#else
void RInterp_Suicide(char*aMsg) { 
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	[[RInterpreter sharedInterpreter] suicide:aMsg]; 
	[pool release];
}
void RInterp_ShowMessage(char*aMsg) { 
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	[[RInterpreter sharedInterpreter] showMessage:aMsg]; 
	[pool release];
}
void RInterp_FlushConsole() { 
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	[[RInterpreter sharedInterpreter] flushConsole]; 
	[pool release];
}
int  RInterp_ReadConsole(char*prompt,unsigned char*buffer,int length,int history) {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	int ret = [[RInterpreter sharedInterpreter] readConsoleWithPrompt:prompt buffer:buffer length:length history:history];
	[pool release];
	return ret;
}
void RInterp_ResetConsole() { [[RInterpreter sharedInterpreter] resetConsole]; }
void RInterp_WriteConsole(char*buffer,int length) { 
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	[[RInterpreter sharedInterpreter] writeConsole:buffer length:length]; 
	[pool release];
}
void RInterp_ClearerrConsole() { [[RInterpreter sharedInterpreter] clearErrorConsole]; }
void RInterp_Busy() { [[RInterpreter sharedInterpreter] busy]; }
void RInterp_CleanUp(SA_TYPE type,int a,int b) {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:NSApplicationWillTerminateNotification object:nil]];
	[[RInterpreter sharedInterpreter] closeInterpreter:nil];
	[pool release];
}
int  RInterp_ShowFiles(int a,char**b,char**c,char*d,Rboolean e,char*f) { 
	return [[RInterpreter sharedInterpreter] showFiles:a file:b headers:c wtitle:d del:e pager:f];
 }
int  RInterp_ChooseFile(int a,char*b,int c) { return 0; }
int  RInterp_EditFile(char*a) { return [[RInterpreter sharedInterpreter] editFile:a]; }
void RInterp_System(char*a) { [[RInterpreter sharedInterpreter] system:a]; }
void RInterp_ProcessEvents() { 
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	[NSApp postEvent:[NSEvent otherEventWithType:NSApplicationDefined 
									location:NSMakePoint(0,0)
									 modifierFlags:0
									 timestamp:0 
									 windowNumber:0 
									 context:nil 
									 subtype:1337 
									 data1:0 
									 data2:0] atStart:NO];
	[NSApp run];
	[pool release];
}
Rboolean RInterp_Device(NewDevDesc *dev,
	char *display,double width,double height,
	double ps,char *family,Rboolean antialias,Rboolean autorefresh,int quartzpos,int bg) {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	Rboolean ret = [[RInterpreter sharedInterpreter] openDevice:dev
	 withDisplay:display width:width height:height pointsize:ps family:family
	  antialias:antialias autorefresh:autorefresh quartzpos:quartzpos background:bg];
	[pool release];
	return ret;
}
void RInterp_DeviceParams(double*a,double*b,double*c,char*d,Rboolean*e,Rboolean*f,int*g) { }
int  RInterp_CustomPrint(char *a,SEXP b) { return [[RInterpreter sharedInterpreter] customPrintType:a list:b]; }

SEXP RInterp_do_selectlist(SEXP call,SEXP op,SEXP args,SEXP rho) {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	SEXP ret = [[RInterpreter sharedInterpreter] selectListWithCall:call op:op args:args rho:rho];
	[pool release];
	return ret;
}

#endif

